#Statikk
--------
Statikk is a League of Legends data analytics platform, providing a suite of tools for players to use to improve and learn about the game.

## Project Setup
*Note: This setup assumes that your development system has Java 8, Maven and NPM installed.*

1. Clone the project locally into the directory of your choosing, using: `git clone https://statikk@bitbucket.org/statikk/statikk.git`
2. Import the root directory into your IDE of choice (this was developed in NetBeans IDE 8.2)
    - The root directory contains a pom.xml file which indicates that this is a multi-module maven project. The three modules are: domain, data-miner, and web-api.
3. Test the projects first, either by running their tests, or by building them (which should automatically run the tests). Running `mvn verify` in the root directory of the project should build and test all projects.
4. For the front-end, open the web-app directory in an IDE that is TypeScript/Angular friendly (NetBeans is *not*). I recommend Visual Studio Code or IntelliJ.
5. From the web-app directory, run `npm install` to install the project's dependencies.
6. From the web-app directory, run `npm start`; the project should compile, and you should be able to open the front end by navigating to http://localhost:4200 in a browser.

## Module Descriptions
### Domain
The domain module contains all Models, DAOs and Services that interact with the database. Additionally, the service that interacts with the Riot API is also in this module (RiotAPIService).

### Web API
The web-api module is a lightweight REST API that produces data for the Angular application (web-app module). Run this module when developing the front-end.

### Data Miner
The data-miner module is an application that will constantly be running, making calls to the Riot API fetching pseudo-random data which is used to aggregate data into the Statikk database. This project does not need to be running for the application to work.

### Web App
The web-app module is the front-end Angular application. The web-app has no Java components, so it is not included in the parent directory's pom file as a module.

### Statikk Database
The database is a MySQL database that was configured with a default user and password. Feel free to change the username/password to one of your choosing (stored in application.properties for development, and supplied as command line parameters for production). The database schema is called `statikk`, and the create table/index/etc. statements are in the file [database/statikk.sql](database/statikk.sql).